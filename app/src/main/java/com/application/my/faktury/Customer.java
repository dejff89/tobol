package com.application.my.faktury;


/**
 * Klasa modelująca atrybuty kliebnta
 */
public class Customer{

    private String payMethod;
    private String nazwaFirmyKlienta;
    private String adresFirmyKlientaUlica;
    private String adresFirmyKlientaKodMiasto;
    private String nipFirmyKlienta;
    private String telefonDoFirmyKlienta;

    public Customer(String payMethod, String nazwaFirmyKlienta, String adresFirmyKlientaUlica, String adresFirmyKlientaKodMiasto, String nipFirmyKlienta, String telefonDoFirmyKlienta) {
        this.payMethod = payMethod;
        this.nazwaFirmyKlienta = nazwaFirmyKlienta;
        this.adresFirmyKlientaUlica = adresFirmyKlientaUlica;
        this.adresFirmyKlientaKodMiasto = adresFirmyKlientaKodMiasto;
        this.nipFirmyKlienta = nipFirmyKlienta;
        this.telefonDoFirmyKlienta = telefonDoFirmyKlienta;
    }

    public String getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }

    public String getNazwaFirmyKlienta() {
        return nazwaFirmyKlienta;
    }

    public void setNazwaFirmyKlienta(String nazwaFirmyKlienta) {
        this.nazwaFirmyKlienta = nazwaFirmyKlienta;
    }

    public String getAdresFirmyKlientaUlica() {
        return adresFirmyKlientaUlica;
    }

    public void setAdresFirmyKlientaUlica(String adresFirmyKlientaUlica) {
        this.adresFirmyKlientaUlica = adresFirmyKlientaUlica;
    }

    public String getAdresFirmyKlientaKodMiasto() {
        return adresFirmyKlientaKodMiasto;
    }

    public void setAdresFirmyKlientaKodMiasto(String adresFirmyKlientaKodMiasto) {
        this.adresFirmyKlientaKodMiasto = adresFirmyKlientaKodMiasto;
    }

    public String getNipFirmyKlienta() {
        return nipFirmyKlienta;
    }

    public void setNipFirmyKlienta(String nipFirmyKlienta) {
        this.nipFirmyKlienta = nipFirmyKlienta;
    }

    public String getTelefonDoFirmyKlienta() {
        return telefonDoFirmyKlienta;
    }

    public void setTelefonDoFirmyKlienta(String telefonDoFirmyKlienta) {
        this.telefonDoFirmyKlienta = telefonDoFirmyKlienta;
    }
}
