package com.application.my.faktury;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasa obsługująca widok podglądu PDF'ów
 */
public class PreviewActivity extends AppCompatActivity {

    private DBHelper dbHelper;
    private ListView pdfList;
    private PdfAdapter pdfAdapter;
    private ArrayList<Faktura> faktury;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        listAll();
        super.onResume();
    }


    //funkcja listująca wygenerowane pdf-y
    private void listAll() {
        pdfList = findViewById(R.id.pdfList);
        dbHelper = new DBHelper(this);
        Cursor data = dbHelper.listujWszystkie();
        faktury = new ArrayList<>();

        if (data.getCount() == 0) {
            Toast.makeText(this, "Nie ma żadnych PDF'ów", Toast.LENGTH_LONG).show();
            pdfAdapter = new PdfAdapter(this, faktury);
            pdfList.setAdapter(pdfAdapter);
        } else {
            while (data.moveToNext()) {
                Long id = data.getLong(0);
                String sciezka = data.getString(1);
                String nrFaktury = data.getString(2);
                Faktura faktura = new Faktura(id, nrFaktury, sciezka);
                faktury.add(faktura);
                pdfAdapter = new PdfAdapter(this, faktury);
                pdfList.setAdapter(pdfAdapter);
            }
        }

        dbHelper.close();
    }

    public void toPreview(String path) {
        File file = new File(path.substring(1));

        if(file.exists()){

            //pobieramy adres pliku
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    Toast.makeText(this, "Nie można zapisać do pamięci", Toast.LENGTH_LONG).show();
                }
                else{
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
            }else {
                Uri uri = Uri.fromFile(file);

                //wywołujemy obiekt intent, przekazujemy mu parmetr, ACTION_VIEW, mówiące że ma zająć się plikiem ze ścieżki uri
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri,"application/pdf");       //ustawiamy typ MIME na application/pdf

                //Sprawdzamy czy jest zainstalowana aplikacja mogąca otworzyć nasz plik
                PackageManager pm = getPackageManager();
                List<ResolveInfo> activities = pm.queryIntentActivities(intent, 0);
                if (activities.size() > 0) {
                    startActivity(intent);      //uruchamiamy aktywność otwierającą pdf-y
                } else {
                    Toast.makeText(this,"Nie ma zainstalowanej aplikacji mogącej otworzyć plik", Toast.LENGTH_LONG).show();
                }
            }

        }else{
            Toast.makeText(this,"Nie ma takiego pliku", Toast.LENGTH_LONG);
        }
    }
}
