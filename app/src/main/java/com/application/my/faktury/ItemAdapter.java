package com.application.my.faktury;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Klasa zarządzająca rozmieszczeniem i działaniem elementów jednej pozycji z listy produktów
 */

public class ItemAdapter extends ArrayAdapter<Item> {
    public ItemAdapter(@NonNull Context context, ArrayList<Item> items) {
        super(context,0, items);
        this.context = context;
    }

    private ArrayList<Item> items;
    private Button deleteButton;
    private Context context;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        Item item = getItem(position);

        if(convertView==null){
            convertView= LayoutInflater.from(getContext()).inflate(R.layout.item, parent, false);
        }

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView quantity = (TextView) convertView.findViewById(R.id.quantity);
        TextView price = (TextView) convertView.findViewById(R.id.price);
        deleteButton = (Button) convertView.findViewById(R.id.deleteBtn);

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Positions)context).deleteItem(position);  //wywołujemy metodę delete item
            }
        });

        name.setText(item.getName());           //ustawiamy polu name wartość name z obiektu item
        quantity.setText(item.getQuantity()+"");//robimy to samo z polem ilość
        price.setText(item.getPrice()+"");      //i z ceną


        return convertView;
    }

}
