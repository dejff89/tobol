package com.application.my.faktury;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


/**
 * Klasa zarządzająca widokiem dodawania pojedynczego artykułu lub usługi do faktury
 */
public class AddPosition extends AppCompatActivity {

    private EditText posName, price, quantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_position);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.posName = (EditText) findViewById(R.id.name);
        this.quantity = (EditText) findViewById(R.id.quantity);
        this.price = (EditText) findViewById(R.id.price);
    }

    public void add(View view){
        if(posName.getText().toString().length()!=0 && quantity.getText().toString().length()!=0 && price.getText().toString().length()!=0){
            Intent intent = new Intent();
            intent.putExtra("positionName", posName.getText().toString());
            intent.putExtra("quantity", quantity.getText().toString());
            intent.putExtra("price", price.getText().toString());
            setResult(1, intent);
            finish();
        }else {
            Toast.makeText(this, "Proszę uzupełnić wszystkie pola",Toast.LENGTH_LONG).show();
        }
    }

}
