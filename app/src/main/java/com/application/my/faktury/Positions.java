package com.application.my.faktury;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Klasa obsługująca widok z listą przedmiotów do sprzedaży
 */

public class Positions extends AppCompatActivity {

    private static ArrayList<Item> positions = new ArrayList<>();
    private ItemAdapter adapter;
    private String positionName, quantity, price;
    private Item item;
    private String payMethod, city, nazwaFirmyKlienta, adresFirmyKlientaUlica, adresFirmyKlientaKodMiasto, nipFirmyKlienta, telefonDoFirmyKlienta;
    private Customer customer;


    private ListView positionList;

    @Override
    protected void onStart() {
        System.out.println("on start");
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        positionList = (ListView) findViewById(R.id.positionList);
        adapter.notifyDataSetChanged();
        adapter = new ItemAdapter(this, positions);
        positionList.setAdapter(adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.city = getIntent().getExtras().getString("city");
        this.payMethod = getIntent().getExtras().getString("payMethod");
        this.nazwaFirmyKlienta = getIntent().getExtras().getString("nazwaFirmyKlienta");
        this.adresFirmyKlientaUlica = getIntent().getExtras().getString("adresFirmyKlientaUlica");
        this.adresFirmyKlientaKodMiasto = getIntent().getExtras().getString("adresFirmyKlientaKodMiasto");
        this.nipFirmyKlienta = getIntent().getExtras().getString("nipFirmyKlienta");
        this.telefonDoFirmyKlienta = getIntent().getExtras().getString("telefonDoFirmyKlienta");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_positions);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        positionList = (ListView) findViewById(R.id.positionList);
        adapter = new ItemAdapter(this, positions);
        positionList.setAdapter(adapter);

        customer = new Customer(payMethod, nazwaFirmyKlienta, adresFirmyKlientaUlica, adresFirmyKlientaKodMiasto, nipFirmyKlienta, telefonDoFirmyKlienta);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void addPosition(View view){
        Intent intent = new Intent(Positions.this, AddPosition.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1){
            this.positionName = data.getStringExtra("positionName");
            this.quantity = data.getStringExtra("quantity");
            this.price = data.getStringExtra("price");
            item = new Item(positionName, Double.valueOf(quantity), Double.valueOf(price));
            positions.add(item);
        }
    }

    public void deleteItem(int position){
        positions.remove(position);
        onResume();
    }

    public void generatePdf(View view) {
        if(positions.size()>0){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    Toast.makeText(this, "Nie można zapisać do pamięci", Toast.LENGTH_LONG).show();
                }
                else{
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
            }
            else{
                new PDFGenerator().createPDF(this, positions, customer, city, this);
                powrotDoPoczatku();
            }

        }
    }

    public void powrotDoPoczatku(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
