package com.application.my.faktury;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

/**
 * Klasa generująca PDF'y
 */

public class PDFGenerator {

    private DBHelper dbHelper;
    private int tmpNrFaktury;
    private SharedPreferences preferences;
    private final String PREFERENCES_NAME = "fakturyUstawienia";
    private static final String COMPANY_NAME = "companyName";
    private static final String COMPANY_STREET = "companyStreet";
    private static final String COMPANY_POSTCODE_CITY = "companyPostcodeCity";
    private static final String COMPANY_NIP = "companyNip";
    private static final String ACCOUNT_NUMBER = "accountNumber";
    private static final String PHONE_NUMBER = "phoneNumber";
    private static final String EMAIL = "email";
    private String nazwaFirmy, adresUlica, adresKodMiasto, nip, numerKonta, numerTelefonu, email, numerFaktury;

    public void createPDF(Activity activity, ArrayList<Item> lista, Customer customer, String city, Context context){

        preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);

        dbHelper = new DBHelper(context);

        this.numerFaktury = preferences.getString("numerFaktury", "1");
        this.nazwaFirmy = preferences.getString(COMPANY_NAME, "");
        this.adresUlica = preferences.getString(COMPANY_STREET, "");
        this.adresKodMiasto = preferences.getString(COMPANY_POSTCODE_CITY, "");
        this.nip = preferences.getString(COMPANY_NIP, "");
        this.numerKonta = preferences.getString(ACCOUNT_NUMBER, "");
        this.numerTelefonu = preferences.getString(PHONE_NUMBER, "");
        this.email = preferences.getString(EMAIL, "");


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat headerSdf = new SimpleDateFormat("MM/yy");
        String headerDate = headerSdf.format(new Date());
        tmpNrFaktury = Integer.valueOf(numerFaktury);
        String pelnyNumerFaktury = tmpNrFaktury+"/"+headerDate;

        try{
            String path = Environment.getExternalStorageDirectory()+"/faktury";
            File file = new File(path+"/",pelnyNumerFaktury.replaceAll("/","_")+".pdf");


            if(ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                if(ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    Toast.makeText(context, "Nie można zapisać do pamięci", Toast.LENGTH_LONG).show();
                }
                else{
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    if(!file.exists()){
                        file.getParentFile().mkdirs();
                        file.createNewFile();
                    }
                }
            }

            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(file.getAbsoluteFile()));
            document.open();
            BaseFont baseFont = BaseFont.createFont("assets/arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(baseFont, 12, Font.NORMAL);
            Font headerFont = new Font(baseFont, 18, Font.BOLD);
            Font contentFont = new Font(baseFont, 10, Font.NORMAL);
            Font tableHeaderFont = new Font(baseFont, 10, Font.BOLD);


            PdfPTable table;
            PdfPCell cell;
            PdfPCell noBorderCell = new PdfPCell();
            noBorderCell.setBorder(Rectangle.NO_BORDER);
            Paragraph p;
            float leading = 10;


            tmpNrFaktury++;

            p = new Paragraph();
            String date = sdf.format(new Date());
            Chunk chunk = new Chunk(", "+date, contentFont);
            p = new Paragraph(city, contentFont);
            p.add(chunk);
            p.setAlignment(Element.ALIGN_RIGHT);
            p.setLeading(leading);
            document.add(p);

            table = new PdfPTable(2);
            table.setWidthPercentage(100);
            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            p = new Paragraph(leading, "Faktura nr "+pelnyNumerFaktury, headerFont);
            p.setAlignment(Element.ALIGN_CENTER);
            cell.addElement(p);
            cell.setColspan(2);
            table.addCell(cell);

            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            p = new Paragraph(leading, "Data wystawienia faktury: "+date, contentFont);
            p.add(Chunk.NEWLINE);
            p.add(new Chunk("Data wykonania usługi: "+date, contentFont));
            p.add(Chunk.NEWLINE);
            p.add(new Chunk("Data sprzedaży: "+date, contentFont));
            p.setAlignment(Element.ALIGN_RIGHT);
            cell.addElement(p);
            table.addCell(cell);

            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            p = new Paragraph(leading, "Sprzedający:", tableHeaderFont);
            p.add(Chunk.NEWLINE);
            p.add(new Chunk(nazwaFirmy, contentFont));
            p.add(Chunk.NEWLINE);
            p.add(new Chunk(adresUlica, contentFont));
            p.add(Chunk.NEWLINE);
            p.add(new Chunk(adresKodMiasto, contentFont));
            p.add(Chunk.NEWLINE);
            p.add(new Chunk(nip, contentFont));
            p.add(Chunk.NEWLINE);
            p.add(new Chunk(numerTelefonu, contentFont));
            p.add(Chunk.NEWLINE);
            p.add(new Chunk(email, contentFont));
            cell.addElement(p);
            table.addCell(cell);

            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            p = new Paragraph(leading, "Kupujący:", tableHeaderFont);
            p.add(Chunk.NEWLINE);
            p.add(new Chunk(customer.getNazwaFirmyKlienta(), contentFont));
            p.add(Chunk.NEWLINE);
            p.add(new Chunk(customer.getAdresFirmyKlientaUlica(), contentFont));
            p.add(Chunk.NEWLINE);
            p.add(new Chunk(customer.getAdresFirmyKlientaKodMiasto(), contentFont));
            p.add(Chunk.NEWLINE);
            p.add(new Chunk(customer.getNipFirmyKlienta(), contentFont));
            p.add(Chunk.NEWLINE);
            p.add(new Chunk(customer.getTelefonDoFirmyKlienta(), contentFont));
            cell.addElement(p);
            table.addCell(cell);

            document.add(table);



            //tabelka
            table = new PdfPTable(new float[]{3,20,3,3,3,3,3});
            table.setWidthPercentage(100);
            table.setSpacingBefore(15);

            cell = new PdfPCell(new Paragraph(leading, "Lp.", tableHeaderFont));
            cell.setBorderWidth(0.8f);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(leading, "Nazwa produktu", tableHeaderFont));
            cell.setBorderWidth(0.8f);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(leading, "Wartość jedn. netto", tableHeaderFont));
            cell.setBorderWidth(0.8f);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(leading, "Ilość", tableHeaderFont));
            cell.setBorderWidth(0.8f);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(leading, "Wartość towaru netto", tableHeaderFont));
            cell.setBorderWidth(0.8f);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(leading, "VAT [%]", tableHeaderFont));
            cell.setBorderWidth(0.8f);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(leading, "Wartość towaru brutto", tableHeaderFont));
            cell.setBorderWidth(0.8f);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            ArrayList<String> sumaWartoscTowNetto = new ArrayList<>();
            ArrayList<String> sumaWartoscTowBrutto = new ArrayList<>();

            int index=1;
            //pętla do listowania pozycji
            for(Item item : lista){

                double wartoscJednNetto = item.getPrice();
                double ilosc = item.getQuantity();
                double wartoscTowNetto, wartoscTowBrutto;


                wartoscTowNetto = ilosc * wartoscJednNetto;
                String wartoscTowNettoWyswietlana = String.format("%.2f", (double)wartoscTowNetto);
                sumaWartoscTowNetto.add(wartoscTowNettoWyswietlana);

                wartoscTowBrutto = wartoscTowNetto * 1.22;
                String wartoscTowBruttoWyswietlana = String.format("%.2f", (double)wartoscTowBrutto);
                sumaWartoscTowBrutto.add(wartoscTowBruttoWyswietlana);


                //Lp.
                cell = new PdfPCell(new Paragraph(leading, index+"", contentFont));
                cell.setBorderWidth(0.8f);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                //Nazwa produktu
                cell = new PdfPCell(new Paragraph(leading, item.getName(), contentFont));
                cell.setBorderWidth(0.8f);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                //Wartość jedn. netto
                cell = new PdfPCell(new Paragraph(leading, item.getPrice()+"", contentFont));
                cell.setBorderWidth(0.8f);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                //Ilość
                cell = new PdfPCell(new Paragraph(leading, item.getQuantity()+"", contentFont));
                cell.setBorderWidth(0.8f);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                //Wartość towaru netto
                cell = new PdfPCell(new Paragraph(leading, wartoscTowNettoWyswietlana+"", contentFont));
                cell.setBorderWidth(0.8f);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                //VAT
                cell = new PdfPCell(new Paragraph(leading, "22", contentFont));
                cell.setBorderWidth(0.8f);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                //Wartość towaru brutto
                cell = new PdfPCell(new Paragraph(leading, wartoscTowBruttoWyswietlana+"", contentFont));
                cell.setBorderWidth(0.8f);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                index++;

            }

            double sumaNetto=0, sumaBrutto=0;
            String sumaNettoWyswietlana, sumaBruttoWyswietlana;
            for(String s : sumaWartoscTowNetto){
                sumaNetto+=Double.parseDouble(s.replace(",","."));
            }

            sumaNettoWyswietlana = String.format(Locale.getDefault() ,"%.2f", (double)sumaNetto);

            for(String s : sumaWartoscTowBrutto){
                sumaBrutto+=Double.parseDouble(s.replace(",", "."));
            }

            sumaBruttoWyswietlana = String.format(Locale.getDefault(),"%.2f", (double)sumaBrutto);

            table.addCell(new PdfPCell(noBorderCell));
            table.addCell(new PdfPCell(noBorderCell));
            table.addCell(new PdfPCell(noBorderCell));

            cell = new PdfPCell(new Paragraph("SUMA"));
            cell.setBorderWidth(0.8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(sumaNettoWyswietlana, contentFont));
            cell.setBorderWidth(0.8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(""));
            cell.setBorderWidth(0.8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.BLACK);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(sumaBruttoWyswietlana, contentFont));
            cell.setBorderWidth(0.8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            document.add(table);

            String[] grosze = sumaBruttoWyswietlana.split(",");

            table = new PdfPTable(1);

            table.setSpacingBefore(20);

            cell = new PdfPCell(new Paragraph("Do zapłaty: "+sumaBruttoWyswietlana, tableHeaderFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setVerticalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph("Słownie: "+LiczbyNaSlowa.translacja((long)sumaBrutto)+" złotych "+grosze[1]+"/100 groszy", contentFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setVerticalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph("Metoda płatności: "+customer.getPayMethod(), contentFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setVerticalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            if("przelew".equals(customer.getPayMethod())){

                cell = new PdfPCell(new Paragraph("Na numer konta: "+ numerKonta, tableHeaderFont));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
            }

            document.add(table);

            table = new PdfPTable(2);
            table.setSpacingBefore(20);
            cell = new PdfPCell(noBorderCell);
            p = new Paragraph(".................................");
            cell.addElement(p);
            table.addCell(cell);

            cell = new PdfPCell(noBorderCell);
            p = new Paragraph(".................................");
            cell.addElement(p);
            table.addCell(cell);

            cell = new PdfPCell(noBorderCell);
            p = new Paragraph("Podpis osoby upoważnionej\n do odbioru faktury");
            cell.addElement(p);
            table.addCell(cell);

            cell = new PdfPCell(noBorderCell);
            p = new Paragraph("Podpis osoby wystawiającej\n fakturę");
            cell.addElement(p);
            table.addCell(cell);

            document.add(table);

            document.close();

            dbHelper.dodajFakturę(new Faktura(pelnyNumerFaktury, file.getAbsolutePath()));

            numerFaktury = String.valueOf(tmpNrFaktury);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("numerFaktury", numerFaktury);
            editor.apply();

            Toast.makeText(context, "Zrobione, zapisano w "+file.getAbsolutePath(), Toast.LENGTH_LONG).show();

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }


}
