package com.application.my.faktury;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;


/**
 * Klasa umożliwiająca listowanie w GUI danych z bazy, w tym przypadku jest to lista wygenerowanych faktur
 */
public class FakturyCursorAdapter extends CursorAdapter{

    public FakturyCursorAdapter(Context context, Cursor cursor){
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.pdf_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

    }
}
