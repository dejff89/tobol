package com.application.my.faktury;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

/**
 * Klasa łącząca customowy widok z danymi pdf, służy ona do poprawnego wyświetlania listy wygenerowanych pdf'ów
 */

public class PdfAdapter extends ArrayAdapter<Faktura> {

    private Button deleteButton, previewButton;
    private TextView nrFakturyET;
    private DBHelper dbHelper;
    private String nrFaktury;
    private Context context;

    public PdfAdapter(@NonNull Context context, ArrayList<Faktura> faktury) {
        super(context, 0, faktury);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View view, @NonNull final ViewGroup parent) {

        Faktura faktura = getItem(position);

        if (view==null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.pdf_item, parent, false);
        }

        dbHelper = new DBHelper(parent.getContext());       //tworzymy obiekt DBHelper

        nrFakturyET = (TextView)view.findViewById(R.id.nrFaktury);
        nrFakturyET.setText(faktura.getNrFaktury());

        deleteButton = (Button)view.findViewById(R.id.usunFaktureBtn);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Faktura faktura = getItem(position); //faktury z listy,
                if(faktura!=null){
                    dbHelper.usunFakture(faktura.getId());    //wysyłamy zapytanie usuwające element o przekazanym id
                    ((PreviewActivity)context).onResume(); //wywołyjemy metodę onResume z MainActivity, która odświeża listę
                    File file = new File(faktura.getSciezkaDoFaktury());
                    boolean deleted = file.delete();
                }
            }
        });

        previewButton = (Button)view.findViewById(R.id.previewBtn);
        previewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Faktura faktura = getItem(position);
                ((PreviewActivity)context).toPreview(faktura.getSciezkaDoFaktury());

            }
        });

        return view;
    }

}
