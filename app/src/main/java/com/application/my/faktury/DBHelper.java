package com.application.my.faktury;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Klasa zawierająca metody umożliwiające wykonywanie zapytań w bazie danych
 */
public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, NAZWA_BD, null, 1);
    }

    public static final String NAZWA_BD = "faktury.db";
    public static final String NAZWA_TABELI = "lista_faktur";
    public static final String ID_COL = "id";
    public static final String KOL_SCIEZKA = "sciezka_do_faktury";
    public static final String KOL_NR_FAKTURY = "numer_faktury";

    //tworzenie tabeli jeśi jeszcze nie istnieje
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + NAZWA_TABELI + " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                KOL_SCIEZKA+" TEXT,"+KOL_NR_FAKTURY+" ilosc TEXT)";
        db.execSQL(createTable);
    }

    //update tabeli
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP IF TABLE EXISTS " + NAZWA_TABELI);
        onCreate(db);
    }

    public Cursor listujWszystkie(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("SELECT * FROM " + NAZWA_TABELI, null);
        return data;
    }

    /*
     * Dodawanie danych o fakturze do BD, nie zapisujemy całych faktur bo SQLite nie radzi sobie zbyt dobrze z BLOB'ami
     */
    public long dodajFakturę(Faktura faktura) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KOL_SCIEZKA, faktura.getSciezkaDoFaktury());
        contentValues.put(KOL_NR_FAKTURY, faktura.getNrFaktury());

        long result = db.insert(NAZWA_TABELI, null, contentValues);

        return result;
    }

    /*
     * Funkcja usuwająca elementy z listy
     */
    public void usunFakture(Long id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(NAZWA_TABELI, ID_COL+"="+id,null);
    }

}
