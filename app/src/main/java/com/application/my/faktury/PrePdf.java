package com.application.my.faktury;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;


/**
 * Klasa obsługująca widok, uzupełniania danuch o kliencie, sposobie płatności, itp.
 */
public class PrePdf extends AppCompatActivity {

    private static final String[] payMethods = {"gotówka", "przelew"};

    private Spinner payMethodsSpinner;
    private EditText city, nazwaFirmyKlientaET, adresFirmyKlientaUlicaET, adresFirmyKlientaKodMiastoET, nipKlientaET, telefonKlientaET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_pdf);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        city = (EditText)findViewById(R.id.city);
        payMethodsSpinner = (Spinner)findViewById(R.id.payMethod);
        nazwaFirmyKlientaET = (EditText)findViewById(R.id.nazwaFirmyKlienta);
        adresFirmyKlientaUlicaET = (EditText)findViewById(R.id.adresFirmyKlientaUlica);
        adresFirmyKlientaKodMiastoET = (EditText)findViewById(R.id.adresFirmyKlientaKodMiasto);
        nipKlientaET = (EditText)findViewById(R.id.nipFirmyKlienta);
        telefonKlientaET = (EditText)findViewById(R.id.telefonDoFirmyKlienta);

        payMethodsSpinner.setPrompt("Wybierz metodę płatności");

    }


    //przxechodzimy do następnej aktwności i przekazujemy parametry
        public void goToFields(View view){
            Intent intent = new Intent(PrePdf.this, Positions.class);
            intent.putExtra("payMethod", payMethodsSpinner.getSelectedItem().toString());
            intent.putExtra("city", city.getText().toString());
            intent.putExtra("nazwaFirmyKlienta", nazwaFirmyKlientaET.getText().toString());
            intent.putExtra("adresFirmyKlientaUlica", adresFirmyKlientaUlicaET.getText().toString());
            intent.putExtra("adresFirmyKlientaKodMiasto", adresFirmyKlientaKodMiastoET.getText().toString());
            intent.putExtra("nipFirmyKlienta", nipKlientaET.getText().toString());
            intent.putExtra("telefonDoFirmyKlienta", telefonKlientaET.getText().toString());
            startActivity(intent);
        }

}
