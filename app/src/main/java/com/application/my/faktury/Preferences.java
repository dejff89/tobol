package com.application.my.faktury;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Klasa obsługująca zapis danych sprzedającego do SharedPreferences
 */
public class Preferences extends AppCompatActivity {

    private SharedPreferences preferences;
    private static final String PREFERENCES_NAME = "fakturyUstawienia";
    private static final String COMPANY_NAME = "companyName";
    private static final String COMPANY_STREET = "companyStreet";
    private static final String COMPANY_POSTCODE_CITY = "companyPostcodeCity";
    private static final String COMPANY_NIP = "companyNip";
    private static final String ACCOUNT_NUMBER = "accountNumber";
    private static final String PHONE_NUMBER = "phoneNumber";
    private static final String EMAIL = "email";

    private EditText companyName, companyStreet, companyPostcodeCity, companyNip, accountNumber, phoneNumber, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        preferences = this.getSharedPreferences(PREFERENCES_NAME,MODE_PRIVATE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.companyName = (EditText) findViewById(R.id.companyName);
        this.companyStreet = (EditText) findViewById(R.id.street);
        this.companyPostcodeCity = (EditText) findViewById(R.id.postcodeCity);
        this.companyNip = (EditText) findViewById(R.id.nip);
        this.accountNumber = (EditText) findViewById(R.id.accountNumber);
        this.phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        this.email = (EditText) findViewById(R.id.email);
        getPrefs();

    }

    public void getPrefs(){
        this.companyName.setText(preferences.getString(COMPANY_NAME, ""));
        this.companyStreet.setText(preferences.getString(COMPANY_STREET, ""));
        this.companyPostcodeCity.setText(preferences.getString(COMPANY_POSTCODE_CITY, ""));
        this.companyNip.setText(preferences.getString(COMPANY_NIP, ""));
        this.accountNumber.setText(preferences.getString(ACCOUNT_NUMBER, ""));
        this.phoneNumber.setText(preferences.getString(PHONE_NUMBER, ""));
        this.email.setText(preferences.getString(EMAIL, ""));
    }

    public void savePreferences(View view){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(COMPANY_NAME, companyName.getText().toString());
        editor.putString(COMPANY_STREET, companyStreet.getText().toString());
        editor.putString(COMPANY_POSTCODE_CITY, companyPostcodeCity.getText().toString());
        editor.putString(COMPANY_NIP, companyNip.getText().toString());
        editor.putString(ACCOUNT_NUMBER, accountNumber.getText().toString());
        editor.putString(PHONE_NUMBER, phoneNumber.getText().toString());
        editor.putString(EMAIL, email.getText().toString());

        if(editor.commit()){
            Toast.makeText(this, "Zapisano ustawienia",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Preferences.this, MainActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(this, "Wystąpił błąd przy zapisie ustawień",Toast.LENGTH_LONG).show();
        };

    }

}
