package com.application.my.faktury;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


/**
 * Klasa głównej aktywności, od niej rozpoczyna działanie aplikacja
 */
public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void toPrePdf(View view){
        Intent intent = new Intent(MainActivity.this, PrePdf.class);
        startActivity(intent);
    }

    public void toProperties(View view){
        Intent intent = new Intent(MainActivity.this, Preferences.class);
        startActivity(intent);
    }

    public void toPreview(View view){
        Intent intent = new Intent(MainActivity.this, PreviewActivity.class);
        startActivity(intent);
    }

}
