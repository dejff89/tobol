package com.application.my.faktury;


/**
 * Klasa modelująca fakturę
 */
public class Faktura {

    public Faktura(){};

    public Faktura(String nrFaktury, String sciezkaDoFaktury) {
        this.nrFaktury = nrFaktury;
        this.sciezkaDoFaktury = sciezkaDoFaktury;
    }

    public Faktura(Long id, String nrFaktury, String sciezkaDoFaktury) {
        this.id = id;
        this.nrFaktury = nrFaktury;
        this.sciezkaDoFaktury = sciezkaDoFaktury;
    }

    private Long id;
    private String nrFaktury;
    private String sciezkaDoFaktury;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNrFaktury() {
        return nrFaktury;
    }

    public void setNrFaktury(String nrFaktury) {
        this.nrFaktury = nrFaktury;
    }

    public String getSciezkaDoFaktury() {
        return sciezkaDoFaktury;
    }

    public void setSciezkaDoFaktury(String sciezkaDoFaktury) {
        this.sciezkaDoFaktury = sciezkaDoFaktury;
    }
}
